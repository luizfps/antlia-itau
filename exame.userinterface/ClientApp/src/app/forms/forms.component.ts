import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Produto, MovimentosManuais, ProdutoCosif } from '../home/home.component';
import { HttpClient } from '@angular/common/http';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-forms',
  templateUrl: './forms.component.html',
  styleUrls: ['./forms.component.css']
})
export class FormsComponent implements OnInit {

  public produtos: Produto[] = [];
  public produtosCosif: ProdutoCosif[] = [];
  public movimentoManual: MovimentosManuais = new MovimentosManuais("", "");
  public disabledInputs: boolean = true;
  private http: HttpClient;
  @Output() updateData: EventEmitter<any> = new EventEmitter();


  constructor(http: HttpClient, private fb: FormBuilder, private toastr:ToastrService) {
    this.http = http
  }

  movimentoManualForm = this.fb.group({
    mes: [, [Validators.required, Validators.min(1), Validators.max(12)]],
    produto: ['', [Validators.required, ]],
    valor: ['', [Validators.required, Validators.min(0)]],
    descricao: ['', [Validators.required]],
    ano: ['', [Validators.required]],
    cosif: ['', [Validators.required]]
  })

  ngOnInit(): void {

    this.http.get<Produto[]>('https://localhost:44385/api/Produto').subscribe(result => {
      this.produtos = result.map(x => new Produto(x.codigoProduto, x.status, x.descricaoProduto));
    }, error => console.error(error));

    this.movimentoManualForm.get('mes').disable({ onlySelf: true })
    this.movimentoManualForm.get('ano').disable({ onlySelf: true })
    this.movimentoManualForm.get('descricao').disable({ onlySelf: true })
    this.movimentoManualForm.get('valor').disable({ onlySelf: true })
  }


  onChange() {

    console.log(this.movimentoManualForm.get('produto').value)
    this.http.get<ProdutoCosif[]>('https://localhost:44385/api/ProdutoCosif/' + this.movimentoManualForm.get('produto').value).subscribe(result => {
      this.produtosCosif = result.map(x => new ProdutoCosif(x.codigoCosif, x.codigoProduto, x.status, x.codigoClassificacao, x.produto));
    }, error => console.error(error));
  }
  onSubmit() {
    console.log(this.movimentoManualForm.value)
    this.http.post<any>('https://localhost:44385/api/MovimentosManuais', {
      Mes: this.movimentoManualForm.get("mes").value,
      Ano: this.movimentoManualForm.get("ano").value,
      codigoProduto: this.movimentoManualForm.get("produto").value,
      codigoCosif: this.movimentoManualForm.get("cosif").value,
      descricao: this.movimentoManualForm.get("descricao").value,
      valor: this.movimentoManualForm.get("valor").value
    }).subscribe(result => {
      this.ResetarCampos()
      this.movimentoManualForm.get('mes').disable({ onlySelf: true })
      this.movimentoManualForm.get('ano').disable({ onlySelf: true })
      this.movimentoManualForm.get('descricao').disable({ onlySelf: true })
      this.movimentoManualForm.get('valor').disable({ onlySelf: true })
      this.updateData.emit(null);
      this.toastr.success('',"Registro salvo com sucesso!",{
        closeButton:true,
        progressBar:true,
        timeOut:5000
      })
    }, error => {
      this.toastr.error('',"Falha ao salvar registro",{
        closeButton:true,
        progressBar:true,
        timeOut:5000
      })
    });
  }

  NovoMovimentoManual() {
    this.movimentoManualForm.get('mes').enable({ onlySelf: false })
    this.movimentoManualForm.get('ano').enable({ onlySelf: false })
    this.movimentoManualForm.get('descricao').enable({ onlySelf: false })
    this.movimentoManualForm.get('valor').enable({ onlySelf: false })
  }
  ResetarCampos() {
    this.movimentoManualForm.reset()
    this.movimentoManualForm.get('produto').setValue('');
    this.movimentoManualForm.get('cosif').setValue('');
    this.produtosCosif = [];
  }

  IncluirMovimentoManual() {
    this.movimentoManualForm.reset();
  }

  get mes() {
    return this.movimentoManualForm.get('mes');
  }
  get descricao() {
    return this.movimentoManualForm.get('descricao');
  }
  get ano() {
    return this.movimentoManualForm.get('ano');
  }
  get produto() {
    return this.movimentoManualForm.get('produto');
  }
  get cosif() {
    return this.movimentoManualForm.get('cosif');
  }
  get valor() {
    return this.movimentoManualForm.get('valor');
  }
}
