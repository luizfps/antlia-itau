import { Component, Input } from '@angular/core';
import { MovimentosManuais } from '../home/home.component';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-tables',
  templateUrl: './tables.component.html',
  styleUrls: ['./tables.component.css']
})
export class TablesComponent {
  private http: HttpClient;

  constructor(http: HttpClient,private toast:ToastrService) {
    this.http = http
  }
  @Input() headers: any[];
  @Input() data: MovimentosManuais[];
  deletaMovimentoManual(id){
    console.log(id);
    this.http.delete('https://localhost:44385/api/MovimentosManuais/'+id).subscribe(result=>{
      this.data = this.data.filter(x=>x.id!=id)
    this.toast.success('','Registro deletado com sucesso',{
        timeOut:2000,
        progressBar:true,
        
      })
    },error=>{});
  }
}


