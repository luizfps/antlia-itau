import { Component, OnInit, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  public movimentosManuais: MovimentosManuais[];
  public titles: string[] = ["Mês", "Ano", "Código do Produto", " Descriçao do Produto", "NR Lançamento", "Descrição", "Valor","Ação"]
  private http: HttpClient;

  constructor(http: HttpClient) {
    this.http = http
  }

  ngOnInit(): void {
    this.getData();
  }
  getData() {
    this.movimentosManuais = []
    this.http.get<MovimentosManuais[]>('https://localhost:44385/api/MovimentosManuais').subscribe(result => {
      this.movimentosManuais = result.map(x => {
        var movimentosm = new MovimentosManuais(x.codigoProduto, x.codigoCosif);
        movimentosm.id = x.id;
        movimentosm.ano = x.ano;
        movimentosm.mes = x.mes;
        movimentosm.numeroLancamento = x.numeroLancamento;
        movimentosm.valor = x.valor;
        movimentosm.descricao = x.descricao;
        movimentosm.produtoCosif = x.produtoCosif;
        return movimentosm;
      }
      );
      if (this.movimentosManuais.length == 0) this.movimentosManuais = null;
    }, error => {
      console.error(error)
      this.movimentosManuais = null
    }
    );
  }
}

export class MovimentosManuais {
  id?:number;
  mes?: number;
  ano?: number;
  codigoProduto?: string;
  codigoCosif?: string;
  numeroLancamento?: number;
  valor?: number;
  descricao?: string;
  produtoCosif: ProdutoCosif
  constructor(
    codigoProduto: string,
    codigoCosif: string
  ) {
    this.codigoCosif = codigoCosif;
    this.codigoProduto = codigoProduto;
  }
}

export class ProdutoCosif {

  constructor(
    public codigoCosif: string,
    public codigoProduto: string,
    public status: string,
    public codigoClassificacao: string,
    public produto: Produto
  ) { }
}
export class Produto {

  constructor(
    public codigoProduto: string,
    public status: string,
    public descricaoProduto: string,
  ) { }
}
