﻿using System.Collections.Generic;
using exame.domain.Entities;
using exame.domain.Interfaces.Services;
using Microsoft.AspNetCore.Mvc;

namespace exame.application.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProdutoController : ControllerBase
    {
        private readonly IProdutoService _produtoService;

        public ProdutoController(IProdutoService produtoService) => _produtoService = produtoService;
   
        // GET api/produto
        [HttpGet]
        public ActionResult<IEnumerable<Produto>> GetAll() => _produtoService.GetAll();

        // POST api/produto
        [HttpPost]
        public void Insert([FromBody] Produto produto) => _produtoService.Insert(produto);

        // DELETW api/produto/001
        [HttpDelete("{codigoProduto}")]
        public void Delete([FromRoute] string codigoProduto) => _produtoService.Delete(codigoProduto);
    }
}
