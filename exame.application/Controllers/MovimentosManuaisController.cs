﻿using System.Collections.Generic;
using exame.domain.Entities;
using exame.domain.Interfaces.Services;
using Microsoft.AspNetCore.Mvc;

namespace exame.application.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MovimentosManuais : ControllerBase
    {
        private readonly IMovimentoManualService _movimentoManualService;

        public MovimentosManuais(IMovimentoManualService movimentoManualService) => _movimentoManualService = movimentoManualService;
      
        // GET api/MovimentoManual
        [HttpGet]
        public ActionResult<IEnumerable<MovimentoManual>> GetAll()=> _movimentoManualService.GetAll();

        // POST api/MovimentoManual
        [HttpPost]
        public void Post([FromBody] MovimentoManual movimentoManual) => _movimentoManualService.Insert(movimentoManual);

        // DELETE api/MovimentoManual/001
        [HttpDelete("{id}")]
        public void Delete([FromRoute] int id) => _movimentoManualService.DeleteById(id);
    }
}
