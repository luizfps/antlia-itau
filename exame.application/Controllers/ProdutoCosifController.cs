﻿using System.Collections.Generic;
using exame.domain.Entities;
using exame.domain.Interfaces.Services;
using Microsoft.AspNetCore.Mvc;

namespace exame.application.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProdutoCosifController : ControllerBase
    {
        private readonly IProdutoCosifServce _produtoCosifService;

        public ProdutoCosifController(IProdutoCosifServce produtoCosifService)=> _produtoCosifService = produtoCosifService;
    
        // GET api/ProdutoCosif
        [HttpGet]
        public ActionResult<IEnumerable<ProdutoCosif>> GetAll() => _produtoCosifService.GetAll();

        // GET api/ProdutoCosif/5
        [HttpGet("{codigoProduto}")]
        public ActionResult<IEnumerable<ProdutoCosif>> Get(string codigoProduto) => _produtoCosifService.GetByCodigoProduto(codigoProduto);
     
        // POST api/ProdutoCosif
        [HttpPost]
        public void Post([FromBody] ProdutoCosif produtoCosif) => _produtoCosifService.Insert(produtoCosif);

        [HttpDelete("{codigoCosif}")]
        public void Delete([FromRoute] string codigoCosif) => _produtoCosifService.Delete(codigoCosif);
    }
}
