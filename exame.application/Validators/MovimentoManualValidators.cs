﻿using exame.domain.Entities;
using FluentValidation;
namespace exame.application.Validators
{
    public class MovimentoManualValidators:AbstractValidator<MovimentoManual>
    {
        public MovimentoManualValidators()
        {
            RuleFor(x => x.Mes).InclusiveBetween(1,12).WithMessage("Mês deve ser entre 1 e 12.");

            RuleFor(x => x.CodigoCosif).NotEmpty().WithMessage("Cosif deve ser selecionado.");

            RuleFor(x => x.CodigoProduto).NotEmpty().WithMessage("Produto deve ser selecionado.");

        }
    }
}
