﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Linq;
using System.Threading.Tasks;

namespace exame.application.RequestFilters
{
    public class ValidationFilter : IAsyncActionFilter
    {
        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            if (!context.ModelState.IsValid)
            {
                var errorsModelState = context.ModelState.Where(x => x.Value.Errors.Count > 0).ToDictionary(kvp => kvp.Key, kvp => kvp.Value.Errors.Select(x => x.ErrorMessage)).ToArray();
                var errorsResponse = new ErrorResponse();
                foreach (var error in errorsModelState)
                {
                    foreach (var value in error.Value)
                    {
                        var errorModel = new ErrorModel
                        {
                            Message = value
                        };

                        errorsResponse.Errors.Add(errorModel);
                     }
                }
                context.Result = new BadRequestObjectResult(errorsResponse);
                return;
            }
            await next();
        }
    }
}
