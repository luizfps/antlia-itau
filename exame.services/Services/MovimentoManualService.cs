﻿using exame.domain.Entities;
using exame.domain.Interfaces.Repositories;
using exame.domain.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;

namespace exame.services.Services
{
    public class MovimentoManualService:IMovimentoManualService
    {
        private readonly IRepository<MovimentoManual> _repository;
        private const string CodigoUsuario = "TESTE";

        public MovimentoManualService(IRepository<MovimentoManual> repository) => _repository = repository;

        public List<MovimentoManual> GetAll() => _repository.GetAll(new[] {$"{nameof(MovimentoManual.ProdutoCosif)}", $"{nameof(MovimentoManual.ProdutoCosif)}.{nameof(ProdutoCosif.Produto)}" }).ToList();

        public void Insert(MovimentoManual MovimentoManualInserting)
        {
            var ultimoLancamento = _repository.Get(x => x.Ano == MovimentoManualInserting.Ano && x.Mes == MovimentoManualInserting.Mes).OrderBy(x => x.NumeroLancamento).LastOrDefault();
            SetUltimoLancamento(MovimentoManualInserting, ultimoLancamento);
            MovimentoManualInserting.DataMovimento = DateTime.Now;
            MovimentoManualInserting.CodigoUsuario = CodigoUsuario;
            _repository.Insert(MovimentoManualInserting);
        }

        private static void SetUltimoLancamento(MovimentoManual movimentoManualInserting, MovimentoManual ultimoLancamento)
        {
            if (ultimoLancamento != default)
                movimentoManualInserting.NumeroLancamento = ultimoLancamento.NumeroLancamento +1;
            else
                movimentoManualInserting.NumeroLancamento = 1;
        }

        public void DeleteById(int id) => _repository.Delete(x => x.Id == id);
    }
}
