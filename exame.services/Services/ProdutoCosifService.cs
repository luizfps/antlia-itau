﻿using exame.domain.Entities;
using exame.domain.Interfaces.Repositories;
using exame.domain.Interfaces.Services;
using System.Collections.Generic;
using System.Linq;

namespace exame.services.Services
{
    public class ProdutoCosifService:IProdutoCosifServce
    {
        private readonly IRepository<ProdutoCosif> _repository;

        public ProdutoCosifService(IRepository<ProdutoCosif> repository)=> _repository = repository;
      
        public List<ProdutoCosif> GetAll() => _repository.GetAll().ToList();

        public void Insert(ProdutoCosif produtoCosif) => _repository.Insert(produtoCosif);

        public List<ProdutoCosif> GetByCodigoProduto(string codigoProduto) => _repository.Get((x)=>x.CodigoProduto == codigoProduto).ToList();

        public void Delete(string codigoCosif) => _repository.Delete(x => x.CodigoCosif == codigoCosif);
    }
}
