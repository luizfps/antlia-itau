﻿using exame.domain.Entities;
using exame.domain.Interfaces.Repositories;
using exame.domain.Interfaces.Services;
using System.Collections.Generic;
using System.Linq;

namespace exame.services.Services
{
    public class ProdutoService:IProdutoService
    {
        private readonly IRepository<Produto> _repository;

        public ProdutoService(IRepository<Produto> repository) => _repository = repository;
  
        public List<Produto> GetAll() => _repository.GetAll().ToList();

        public void Insert(Produto produto) => _repository.Insert(produto);

        public void Delete(string codigoProduto) => _repository.Delete(x => x.CodigoProduto == codigoProduto);
    }
}
