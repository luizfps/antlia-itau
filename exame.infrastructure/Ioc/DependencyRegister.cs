﻿using exame.domain.Entities;
using exame.domain.Interfaces.Repositories;
using exame.domain.Interfaces.Services;
using exame.infrastructure.Context;
using exame.infrastructure.RepositoryBase;
using exame.services.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace exame.infrastructure.Ioc
{
    public static class DependencyRegister
    {
        public static void Register(this IServiceCollection services,string connectionString)
        {
            services.AddDbContext<MovimentosManuaisContext>(options => options.UseSqlServer(connectionString));
            services.AddScoped(typeof(IRepository<>), typeof(RepositoryBase<>));
            services.AddScoped(typeof(IRepository<Produto>), typeof(ProdutoRepository));
            services.AddScoped(typeof(IRepository<ProdutoCosif>), typeof(ProdutoCosifRepository));
            services.AddScoped(typeof(IRepository<MovimentoManual>), typeof(MovimentoManualRepository));
            services.AddScoped(typeof(IMovimentoManualService), typeof(MovimentoManualService));
            services.AddScoped(typeof(IProdutoCosifServce), typeof(ProdutoCosifService));
            services.AddScoped(typeof(IProdutoService), typeof(ProdutoService));
        }
    }
}
