﻿using exame.domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace exame.infrastructure.Mappings
{
    public class MovimentoManualMap : IEntityTypeConfiguration<MovimentoManual>
    {
       public void Configure(EntityTypeBuilder<MovimentoManual> modelBuilder)
        {

            modelBuilder.ToTable("MOVIMENTO_MANUAL");

            modelBuilder.HasIndex(e => e.CodigoCosif);

            modelBuilder.Property(e => e.Id).HasColumnName("ID");

            modelBuilder.Property(e => e.CodigoCosif)
                    .IsRequired()
                    .HasColumnName("COD_COSIF")
                    .HasMaxLength(11)
                    .IsUnicode(false);

            modelBuilder.Property(e => e.CodigoProduto)
                    .IsRequired()
                    .HasColumnName("COD_PRODUTO")
                    .HasMaxLength(4)
                    .IsUnicode(false);

            modelBuilder.Property(e => e.CodigoUsuario)
                    .IsRequired()
                    .HasColumnName("COD_USUARIO")
                    .HasMaxLength(15)
                    .IsUnicode(false);

            modelBuilder.Property(e => e.Ano)
                    .HasColumnName("DAT_ANO")
                    .HasColumnType("numeric(4, 0)");

            modelBuilder.Property(e => e.Mes)
                    .HasColumnName("DAT_MES")
                    .HasColumnType("numeric(2, 0)");

            modelBuilder.Property(e => e.DataMovimento)
                    .HasColumnName("DAT_MOVIMENTO")
                    .HasColumnType("smalldatetime");

            modelBuilder.Property(e => e.Descricao)
                    .IsRequired()
                    .HasColumnName("DES_DESCRICAO")
                    .HasMaxLength(50)
                    .IsUnicode(false);

            modelBuilder.Property(e => e.NumeroLancamento)
                    .HasColumnName("NUM_LANCAMENTO")
                    .HasColumnType("numeric(18, 0)");

            modelBuilder.Property(e => e.Valor)
                    .HasColumnName("VAL_VALOR")
                    .HasColumnType("numeric(18, 2)");

            modelBuilder.HasOne(d => d.ProdutoCosif)
                    .WithMany(p => p.MovimentoManual)
                    .HasForeignKey(d => d.CodigoCosif); 
        }
    }
}
