﻿using exame.domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace exame.infrastructure.Mappings
{
    public class ProdutoCosifMap : IEntityTypeConfiguration<ProdutoCosif>
    {
        public void Configure(EntityTypeBuilder<ProdutoCosif> entity)
        {
            entity.HasKey(e => e.CodigoCosif);

            entity.ToTable("PRODUTO_COSIF");

            entity.HasIndex(e => e.CodigoProduto);

            entity.Property(e => e.CodigoCosif)
                .HasColumnName("COD_COSIF")
                .HasMaxLength(11)
                .IsUnicode(false)
                .ValueGeneratedNever();

            entity.Property(e => e.CodigoClassificacao)
                .HasColumnName("COD_CLASSIFICACAO")
                .HasMaxLength(6)
                .IsUnicode(false);

            entity.Property(e => e.CodigoProduto)
                .HasColumnName("COD_PRODUTO")
                .HasMaxLength(4)
                .IsUnicode(false);

            entity.Property(e => e.Status)
                .HasColumnName("STA_STATUS")
                .HasMaxLength(1)
                .IsUnicode(false);

            entity.HasMany(d => d.MovimentoManual)
                .WithOne(x=>x.ProdutoCosif)
                .HasForeignKey(d => d.CodigoCosif);

            entity.HasOne(d => d.Produto).WithMany(x => x.ProdutoCosif).HasForeignKey(x => x.CodigoProduto);
        }
    }
}
