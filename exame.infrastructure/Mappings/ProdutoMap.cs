﻿using exame.domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace exame.infrastructure.Mappings
{
    public class ProdutoMap: IEntityTypeConfiguration<Produto>
    {
        public void Configure(EntityTypeBuilder<Produto> entity)
        {

            entity.HasKey(e => e.CodigoProduto);

            entity.ToTable("PRODUTO");

            entity.Property(e => e.CodigoProduto)
                .HasColumnName("COD_PRODUTO")
                .HasMaxLength(4)
                .IsUnicode(false)
                .ValueGeneratedNever();

            entity.Property(e => e.DescricaoProduto)
                .HasColumnName("DES_PRODUTO")
                .HasMaxLength(30)
                .IsUnicode(false);

            entity.Property(e => e.Status)
                .HasColumnName("STA_STATUS")
                .HasMaxLength(1)
                .IsUnicode(false);
        }
    }
}
