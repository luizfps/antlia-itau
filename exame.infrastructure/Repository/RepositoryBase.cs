﻿using exame.domain.Interfaces.Repositories;
using exame.infrastructure.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace exame.infrastructure.RepositoryBase
{
    public class RepositoryBase<T> : IRepository<T>, IDisposable where T : class
    {
        private readonly MovimentosManuaisContext _movimentosManuaisContext;

        public RepositoryBase(MovimentosManuaisContext movimentosManuaisContext) => _movimentosManuaisContext = movimentosManuaisContext;

        private void Commit() => _movimentosManuaisContext.SaveChanges();

        public void Dispose()
        {
            if (_movimentosManuaisContext != null)
            {
                _movimentosManuaisContext.Dispose();
            }
            GC.SuppressFinalize(this);
        }

        public IQueryable<T> Get(Expression<Func<T, bool>> predicate)=> _movimentosManuaisContext.Set<T>().Where(predicate);

        public IQueryable<T> GetAll(string[] expands = default) {
            
            IQueryable<T> query = _movimentosManuaisContext.Set<T>();
            if (expands != default)
            {
                foreach (var exp in expands)
                    query = query.Include(exp);
            }

            return query;
        }
      
        public void Insert(T entity)
        {
            _movimentosManuaisContext.Set<T>().Add(entity);
            Commit();
        }

        public void Delete(Func<T, bool> predicate)
        {
            _movimentosManuaisContext.Set<T>()
           .Where(predicate).ToList()
           .ForEach(del => _movimentosManuaisContext.Set<T>().Remove(del));
            Commit();
        }
    }
}
