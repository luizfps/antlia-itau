﻿using exame.domain.Entities;
using exame.infrastructure.Context;
using System;
using System.Collections.Generic;
using System.Text;

namespace exame.infrastructure.RepositoryBase
{
    class ProdutoRepository : RepositoryBase<Produto>
    {
        public ProdutoRepository(MovimentosManuaisContext movimentosManuaisContext) : base(movimentosManuaisContext)
        {
        }
    }
}
