﻿using exame.domain.Entities;
using exame.infrastructure.Context;

namespace exame.infrastructure.RepositoryBase
{
    class ProdutoCosifRepository : RepositoryBase<ProdutoCosif>
    {
        public ProdutoCosifRepository(MovimentosManuaisContext movimentosManuaisContext) : base(movimentosManuaisContext)
        {
        }
    }
}
