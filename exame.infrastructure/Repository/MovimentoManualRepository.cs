﻿using exame.domain.Entities;
using exame.infrastructure.Context;

namespace exame.infrastructure.RepositoryBase
{
    class MovimentoManualRepository : RepositoryBase<MovimentoManual>
    {
        public MovimentoManualRepository(MovimentosManuaisContext movimentosManuaisContext) : base(movimentosManuaisContext)
        {
        }
    }
}
