﻿using exame.domain.Entities;
using exame.infrastructure.Mappings;
using Microsoft.EntityFrameworkCore;

namespace exame.infrastructure.Context{
    public partial class MovimentosManuaisContext : DbContext
    {
        public MovimentosManuaisContext()
        {
        }

        public MovimentosManuaisContext(DbContextOptions<MovimentosManuaisContext> options)
            : base(options)
        {
        }

        public virtual DbSet<MovimentoManual> MovimentoManual { get; set; }
        public virtual DbSet<Produto> Produto { get; set; }
        public virtual DbSet<ProdutoCosif> ProdutoCosif { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) => base.OnConfiguring(optionsBuilder);

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<MovimentoManual>(new MovimentoManualMap().Configure);
            modelBuilder.Entity<Produto>(new ProdutoMap().Configure);
            modelBuilder.Entity<ProdutoCosif>(new ProdutoCosifMap().Configure);
        }
    }
}
