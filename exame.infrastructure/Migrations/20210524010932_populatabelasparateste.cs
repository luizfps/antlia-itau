﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace exame.infrastructure.Migrations
{
    public partial class populatabelasparateste : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("insert into produto(COD_PRODUTO,DES_PRODUTO) values('002','mesa')");
            migrationBuilder.Sql("insert into produto(COD_PRODUTO,DES_PRODUTO) values('003','cadeira')");
            migrationBuilder.Sql("insert into produto(COD_PRODUTO,DES_PRODUTO) values('004','couchão')");
            migrationBuilder.Sql("insert into produto(COD_PRODUTO,DES_PRODUTO) values('005','televisão')");
            migrationBuilder.Sql("insert into produto(COD_PRODUTO,DES_PRODUTO) values('006','geladeira')");
            migrationBuilder.Sql("insert into produto(COD_PRODUTO,DES_PRODUTO) values('007','sofá')");
            migrationBuilder.Sql("insert into produto(COD_PRODUTO,DES_PRODUTO) values('008','microondas')");
            migrationBuilder.Sql("insert into produto(COD_PRODUTO,DES_PRODUTO) values('009','ps4')");
            migrationBuilder.Sql("insert into produto(COD_PRODUTO,DES_PRODUTO) values('010','xboxone')");
            migrationBuilder.Sql("insert into produto(COD_PRODUTO,DES_PRODUTO) values('011','dvd')");
            migrationBuilder.Sql("insert into produto(COD_PRODUTO,DES_PRODUTO) values('012','smartphone')");
            migrationBuilder.Sql("insert into produto(COD_PRODUTO,DES_PRODUTO) values('013','bicicleta')");

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("delete from produto");
        }
    }
}
