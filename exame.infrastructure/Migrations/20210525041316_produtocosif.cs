﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace exame.infrastructure.Migrations
{
    public partial class produtocosif : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("insert into PRODUTO_COSIF (COD_COSIF,COD_PRODUTO,COD_CLASSIFICACAO) values ('COS01',(SELECT COD_PRODUTO FROM PRODUTO WHERE DES_PRODUTO='mesa'),'clas01')");
            migrationBuilder.Sql("insert into PRODUTO_COSIF (COD_COSIF,COD_PRODUTO,COD_CLASSIFICACAO) values ('COS02',(SELECT COD_PRODUTO FROM PRODUTO WHERE DES_PRODUTO='mesa'),'clas02')");
            migrationBuilder.Sql("insert into PRODUTO_COSIF (COD_COSIF,COD_PRODUTO,COD_CLASSIFICACAO) values ('COS03',(SELECT COD_PRODUTO FROM PRODUTO WHERE DES_PRODUTO='mesa'),'clas03')");
            migrationBuilder.Sql("insert into PRODUTO_COSIF (COD_COSIF,COD_PRODUTO,COD_CLASSIFICACAO) values ('COS04',(SELECT COD_PRODUTO FROM PRODUTO WHERE DES_PRODUTO='ps4'),'clas04')");
            migrationBuilder.Sql("insert into PRODUTO_COSIF (COD_COSIF,COD_PRODUTO,COD_CLASSIFICACAO) values ('COS05',(SELECT COD_PRODUTO FROM PRODUTO WHERE DES_PRODUTO='ps4'),'clas05')");
            migrationBuilder.Sql("insert into PRODUTO_COSIF (COD_COSIF,COD_PRODUTO,COD_CLASSIFICACAO) values ('COS06',(SELECT COD_PRODUTO FROM PRODUTO WHERE DES_PRODUTO='xboxone'),'clas06')");
            migrationBuilder.Sql("insert into PRODUTO_COSIF (COD_COSIF,COD_PRODUTO,COD_CLASSIFICACAO) values ('COS07',(SELECT COD_PRODUTO FROM PRODUTO WHERE DES_PRODUTO='xboxone'),'clas07')");
            migrationBuilder.Sql("insert into PRODUTO_COSIF (COD_COSIF,COD_PRODUTO,COD_CLASSIFICACAO) values ('COS08',(SELECT COD_PRODUTO FROM PRODUTO WHERE DES_PRODUTO='xboxone'),'clas08')");
            migrationBuilder.Sql("insert into PRODUTO_COSIF (COD_COSIF,COD_PRODUTO,COD_CLASSIFICACAO) values ('COS09',(SELECT COD_PRODUTO FROM PRODUTO WHERE DES_PRODUTO='smartphone'),'clas09')");
            migrationBuilder.Sql("insert into PRODUTO_COSIF (COD_COSIF,COD_PRODUTO,COD_CLASSIFICACAO) values ('COS10',(SELECT COD_PRODUTO FROM PRODUTO WHERE DES_PRODUTO='smartphone'),'clas10')");
            migrationBuilder.Sql("insert into PRODUTO_COSIF (COD_COSIF,COD_PRODUTO,COD_CLASSIFICACAO) values ('COS11',(SELECT COD_PRODUTO FROM PRODUTO WHERE DES_PRODUTO='smartphone'),'clas11')");
            migrationBuilder.Sql("insert into PRODUTO_COSIF (COD_COSIF,COD_PRODUTO,COD_CLASSIFICACAO) values ('COS12',(SELECT COD_PRODUTO FROM PRODUTO WHERE DES_PRODUTO='bicicleta'),'clas12')");
            migrationBuilder.Sql("insert into PRODUTO_COSIF (COD_COSIF,COD_PRODUTO,COD_CLASSIFICACAO) values ('COS13',(SELECT COD_PRODUTO FROM PRODUTO WHERE DES_PRODUTO='bicicleta'),'clas13')");
            migrationBuilder.Sql("insert into PRODUTO_COSIF (COD_COSIF,COD_PRODUTO,COD_CLASSIFICACAO) values ('COS14',(SELECT COD_PRODUTO FROM PRODUTO WHERE DES_PRODUTO='bicicleta'),'clas14')");
            migrationBuilder.Sql("insert into PRODUTO_COSIF (COD_COSIF,COD_PRODUTO,COD_CLASSIFICACAO) values ('COS15',(SELECT COD_PRODUTO FROM PRODUTO WHERE DES_PRODUTO='dvd'),'clas15')");
            migrationBuilder.Sql("insert into PRODUTO_COSIF (COD_COSIF,COD_PRODUTO,COD_CLASSIFICACAO) values ('COS16',(SELECT COD_PRODUTO FROM PRODUTO WHERE DES_PRODUTO='dvd'),'clas16')");
            migrationBuilder.Sql("insert into PRODUTO_COSIF (COD_COSIF,COD_PRODUTO,COD_CLASSIFICACAO) values ('COS17',(SELECT COD_PRODUTO FROM PRODUTO WHERE DES_PRODUTO='televisão'),'clas17')");
            migrationBuilder.Sql("insert into PRODUTO_COSIF (COD_COSIF,COD_PRODUTO,COD_CLASSIFICACAO) values ('COS18',(SELECT COD_PRODUTO FROM PRODUTO WHERE DES_PRODUTO='cadeira'),'clas18')");
            migrationBuilder.Sql("insert into PRODUTO_COSIF (COD_COSIF,COD_PRODUTO,COD_CLASSIFICACAO) values ('COS19',(SELECT COD_PRODUTO FROM PRODUTO WHERE DES_PRODUTO='couchão'),'clas19')");
            migrationBuilder.Sql("insert into PRODUTO_COSIF (COD_COSIF,COD_PRODUTO,COD_CLASSIFICACAO) values ('COS20',(SELECT COD_PRODUTO FROM PRODUTO WHERE DES_PRODUTO='couchão'),'clas20')");

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("DELETE FROM PRODUTO_COSIF");
        }
    }
}
