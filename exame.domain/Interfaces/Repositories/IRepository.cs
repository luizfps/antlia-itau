﻿using System;
using System.Linq;
using System.Linq.Expressions;

namespace exame.domain.Interfaces.Repositories
{

    public interface IRepository<T> where T : class
        {
            IQueryable<T> GetAll(string[] expands = default);
            IQueryable<T> Get(Expression<Func<T, bool>> predicate);
            //T Find(params object[] key);
            //T First(Expression<Func<T, bool>> predicate);
            void Insert(T entity);
            //void Update(T entity);
            void Delete(Func<T, bool> predicate);
            void Dispose();
        }
}
