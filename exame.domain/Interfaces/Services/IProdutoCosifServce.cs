﻿using exame.domain.Entities;
using System.Collections.Generic;

namespace exame.domain.Interfaces.Services
{
    public interface IProdutoCosifServce
    {
        List<ProdutoCosif> GetAll();
        void Insert(ProdutoCosif produtoCosif);
        List<ProdutoCosif> GetByCodigoProduto(string codigoProduto);
        void Delete(string codigoCosif);
    }
}
