﻿using exame.domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace exame.domain.Interfaces.Services
{
    public interface IProdutoService
    {
        List<Produto> GetAll();
        void Insert(Produto produto);
        void Delete(string codigoProduto);
    }
}
