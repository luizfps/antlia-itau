﻿using exame.domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace exame.domain.Interfaces.Services
{
    public interface IMovimentoManualService
    {
        List<MovimentoManual> GetAll();
        void Insert(MovimentoManual movimentoManual);
        void DeleteById(int id);
    }
}
