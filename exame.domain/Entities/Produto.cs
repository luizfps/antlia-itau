﻿using System.Collections.Generic;

namespace exame.domain.Entities
{
    public class Produto
    {
        public Produto()
        {
            ProdutoCosif = new HashSet<ProdutoCosif>();
        }

        public string CodigoProduto { get; set; }

        public string DescricaoProduto { get; set; }

        public string Status { get; set; }

        public virtual ICollection<ProdutoCosif> ProdutoCosif { get; set; }
    }
}
