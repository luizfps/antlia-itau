﻿using System.Collections.Generic;

namespace exame.domain.Entities
{
    public class ProdutoCosif
    {
        public ProdutoCosif()
        {
            MovimentoManual = new HashSet<MovimentoManual>();
        }

        public string CodigoCosif { get; set; }

        public string CodigoProduto { get; set; }

        public string CodigoClassificacao { get; set; }

        public string Status { get; set; }

        public virtual Produto Produto{ get; set; }

        public virtual ICollection<MovimentoManual> MovimentoManual { get; set; }
    }
}
